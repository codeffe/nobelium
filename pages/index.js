import Container from '@/components/Container'
import BlogPost from '@/components/BlogPost'
import Pagination from '@/components/Pagination'
import { getAllPosts, getHomeCover } from '@/lib/notion'
import BLOG from '@/blog.config'

export async function getStaticProps () {
  const posts = await getAllPosts({ includePages: false })
  const cover = await getHomeCover()
  const topic = posts.filter(v => v.topic === 'Yes')
  const articles = posts.filter(v => v.topic === 'No')

  const postsToShow = articles.slice(0, BLOG.postsPerPage)
  const totalPosts = posts.length
  const showNext = totalPosts > BLOG.postsPerPage
  return {
    props: {
      page: 1, // current page is 1
      postsToShow,
      topic,
      cover,
      showNext
    },
    revalidate: 1
  }
}

const Cover = ({ cover }) => {
  return (
    <div className="shadow h-80 w-full bg-gray-200 mb-12 rounded-2xl relative bg-center bg-cover" style={{ backgroundImage: 'url(' + cover + ')', backgroundSize: '100%' }}>
      <div className="shadow-lg absolute h-24 w-24 -mt-12 -ml-12 bg-gray-100 rounded-full top-full left-2/4 border-indigo-400 zHead">
        <div className="zLinear h-24 w-24 rounded-full"></div>
      </div>
    </div>
  )
}

const blog = ({ topic, postsToShow, page, showNext, cover }) => {
  // console.log('cover----', cover)
  return (
    <Container title={BLOG.title} description={BLOG.description}>
      {BLOG.isCover && <Cover cover={cover} />}
      <h2 className="text-xl pb-4 ztitle">专题期刊</h2>
      <div className="grid gap-6 lg:grid-cols-4 md:grid-cols-4 sm:grid-cols-1 mb-10">
        {topic.map(post => (
          <BlogPost key={post.id} post={post} type={'topic'} />
        ))}
      </div>
      <h2 className="text-xl ztitle">技术周刊</h2>
      <div className="grid gap-6 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1">
        {postsToShow.map(post => (
          <BlogPost key={post.id} post={post} type={'post'} />
        ))}
      </div>
      {showNext && <Pagination page={page} showNext={showNext} />}
    </Container>
  )
}

export default blog
