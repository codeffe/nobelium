
export const uuidToId = function (id) {
  if (id === undefined) {
    id = ''
  }
  return id.split('-').join('')
}

export const defaultMapImageUrl = (url, block) => {
  // console.log('defaultMapImageUrl---', url);
  if (!url) {
    return null
  }

  if (url.startsWith('data:')) {
    return url
  }

  if (url.startsWith('/images')) {
    url = `https://www.notion.so${url}`
  }

  // more recent versions of notion don't proxy unsplash images
  if (!url.startsWith('https://images.unsplash.com')) {
    url = `https://www.notion.so${
      url.startsWith('/image') ? url : `/image/${encodeURIComponent(url)}`
    }`

    const notionImageUrlV2 = new URL(url)
    let table = block.parent_table === 'space' ? 'block' : block.parent_table
    if (table === 'collection') {
      table = 'block'
    }
    notionImageUrlV2.searchParams.set('table', table)
    notionImageUrlV2.searchParams.set('id', block.id)
    notionImageUrlV2.searchParams.set('cache', 'v2')

    if (url.indexOf('amazonaws.com') > -1) {
      notionImageUrlV2.searchParams.set('userId', block.created_by_id)
      notionImageUrlV2.searchParams.set('spaceId', block.space_id)
      notionImageUrlV2.searchParams.set('width', '800')
    }

    url = notionImageUrlV2.toString()
  }

  return url
}

export const defaultMapPageUrl = (rootPageId) => (pageId) => {
  pageId = (pageId || '').replace(/-/g, '')

  if (rootPageId && pageId === rootPageId) {
    return '/'
  } else {
    return `/${pageId}`
  }
}

const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
]

export const formatDate = (input) => {
  const date = new Date(input)
  const month = date.getMonth()
  return `${months[month]} ${date.getDate()}, ${date.getFullYear()}`
}
