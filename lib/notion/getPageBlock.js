import BLOG from '@/blog.config'
import { NotionAPI } from 'notion-client'
import { idToUuid } from 'notion-utils'
import { defaultMapImageUrl } from '../utils'

export default async function getPageBlock (pid) {
  const authToken = BLOG.notionAccessToken || null
  const api = new NotionAPI({ authToken })
  const res = await api.getPage(pid)
  const pageBlock = res.block[idToUuid(pid)].value
  const pageIcon = pageBlock.format ? pageBlock.format.page_icon : null
  const pageCover = pageBlock.format ? pageBlock.format.page_cover : null
  // console.log('getPageBlock---', pageBlock.format)
  return {
    icon: pageIcon,
    cover: pageCover ? defaultMapImageUrl(pageCover, pageBlock) : null
  }
}
