import BLOG from '@/blog.config'
import { NotionAPI } from 'notion-client'
import { idToUuid } from 'notion-utils'
import getAllPageIds from './getAllPageIds'
import getPageProperties from './getPageProperties'
import filterPublishedPosts from './filterPublishedPosts'

import getPageBlock from './getPageBlock'
import { uuidToId } from '../utils'

export async function getHomeCover () {
  const authToken = BLOG.notionAccessToken || null
  const api = new NotionAPI({ authToken })
  const response = await api.getPage(BLOG.notionPageId)
  const collection = Object.values(response.collection)[0]?.value
  return collection.cover
}

/**
 * @param {{ includePages: boolean }} - false: posts only / true: include pages
 */
export async function getAllPosts ({ includePages = false }) {
  let id = BLOG.notionPageId
  const authToken = BLOG.notionAccessToken || null
  const api = new NotionAPI({ authToken })
  const response = await api.getPage(id)

  id = idToUuid(id)
  const collection = Object.values(response.collection)[0]?.value
  const collectionQuery = response.collection_query
  const block = response.block
  const schema = collection?.schema

  const rawMetadata = block[id].value

  let tagsColor = []

  for (const key in schema) {
    if (schema[key].type === 'multi_select' && schema[key].name === 'tags') {
      tagsColor = schema[key].options
    }
  }

  // console.log('collection---', JSON.stringify(collection, null, 2))

  // Check Type
  if (
    rawMetadata?.type !== 'collection_view_page' &&
    rawMetadata?.type !== 'collection_view'
  ) {
    console.log(`pageId "${id}" is not a database`)
    return null
  } else {
    // Construct Data
    const pageIds = getAllPageIds(collectionQuery)

    // console.log('pageIds---',  JSON.stringify(collectionQuery, null, 2), pageIds)

    const data = []
    for (let i = 0; i < pageIds.length; i++) {
      const id = pageIds[i]
      const properties = (await getPageProperties(id, block, schema)) || null

      // Add fullwidth, createdtime to properties
      properties.createdTime = new Date(
        block[id].value?.created_time
      ).toString()
      properties.fullWidth = block[id].value?.format?.page_full_width ?? false

      const info = (await getPageBlock(uuidToId(id))) || null
      if (info) {
        properties.cover = info.cover
        properties.icon = info.icon
      }

      const tags = properties.tags
      if (tags && tags.length > 0) {
        properties.tagsColor = tagsColor.filter((v) => tags.indexOf(v.value) > -1)
      }

      data.push(properties)
    }

    // remove all the the items doesn't meet requirements
    const posts = filterPublishedPosts({ posts: data, includePages })
    let _topic = []
    let _posts = posts
    // Sort by date
    if (BLOG.sortByDate) {
      _topic = posts.filter(v => v.topic === 'Yes') || []
      _posts = posts
        .filter(v => v.topic === undefined || v.topic === 'No')
        .sort((a, b) => {
          const dateA = new Date(a?.date?.start_date || a.createdTime)
          const dateB = new Date(b?.date?.start_date || b.createdTime)
          return dateB - dateA
        })
    }
    const result = [..._topic, ..._posts]
    // console.log('posts', _topic)
    return result
  }
}
