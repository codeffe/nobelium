# *
User-agent: *
Allow: /

# Host
Host: https://codeffe.vercel.app

# Sitemaps
Sitemap: https://codeffe.vercel.app/sitemap.xml
