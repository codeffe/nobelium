import BLOG from '@/blog.config'
// import Vercel from '@/components/Vercel'
const Footer = ({ fullWidth }) => {
  const d = new Date()
  const y = d.getFullYear()
  const from = +BLOG.since
  return (
    <footer
      className={`bg-gray-900 justify-center py-4 text-center mt-6 flex-shrink-0 m-auto w-full text-gray-500 dark:text-gray-400 transition-all ${
        !fullWidth ? 'px-4' : 'px-4 md:px-24'
      }`}
    >
      <div className="my-4 text-sm leading-6 font-medium">
        <p>
          © {BLOG.author} <span className="px-1">❤</span>{' '}
          {from === y || !from ? y : `${from} - ${y}`}
        </p>
        <p>
          Powered by <a href="https://zhiking.notion.site/6c436d28bbb44b59990a83ba30b0161a?v=79f1458a1ebf4e7689c628749c8471a7">Notion</a> & Vercel.
        </p>
      </div>
    </footer>
  )
}

export default Footer
